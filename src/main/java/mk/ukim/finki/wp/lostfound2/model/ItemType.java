package mk.ukim.finki.wp.lostfound2.model;

import lombok.Getter;

@Getter
public enum ItemType {
    LOST("Изгубено"),
    FOUND("Најдено");

    private final String typeName;

    ItemType(String typeName) {
        this.typeName = typeName;
    }

    public String getTypeName() {
        return typeName;
    }
}
