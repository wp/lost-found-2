package mk.ukim.finki.wp.lostfound2.service;

import mk.ukim.finki.wp.lostfound2.model.dtos.MailSendingStatus;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;


public interface EmailService {

    CompletableFuture<MailSendingStatus> sendMail(String[] to, String subject, String template,
                                                  List<String> cc, Map<String, Object> model, File attachment);
}
