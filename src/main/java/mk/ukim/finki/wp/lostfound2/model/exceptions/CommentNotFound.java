package mk.ukim.finki.wp.lostfound2.model.exceptions;

public class CommentNotFound extends RuntimeException {
    public CommentNotFound(String message) {
        super(message);
    }
}