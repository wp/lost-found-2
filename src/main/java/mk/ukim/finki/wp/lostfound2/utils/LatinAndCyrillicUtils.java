package mk.ukim.finki.wp.lostfound2.utils;

import java.util.HashMap;
import java.util.Map;

public class LatinAndCyrillicUtils {

    private static final Map<Character, String> cyrillicToLatinMap = new HashMap<>();
    private static final Map<String, Character> latinToCyrillicMap = new HashMap<>();

    static {
        cyrillicToLatinMap.put('А', "A");
        cyrillicToLatinMap.put('Б', "B");
        cyrillicToLatinMap.put('В', "V");
        cyrillicToLatinMap.put('Г', "G");
        cyrillicToLatinMap.put('Д', "D");
        cyrillicToLatinMap.put('Ѓ', "Gj");
        cyrillicToLatinMap.put('Е', "E");
        cyrillicToLatinMap.put('Ж', "Zh");
        cyrillicToLatinMap.put('З', "Z");
        cyrillicToLatinMap.put('Ѕ', "Dz");
        cyrillicToLatinMap.put('И', "I");
        cyrillicToLatinMap.put('Ј', "J");
        cyrillicToLatinMap.put('К', "K");
        cyrillicToLatinMap.put('Л', "L");
        cyrillicToLatinMap.put('Љ', "Lj");
        cyrillicToLatinMap.put('М', "M");
        cyrillicToLatinMap.put('Н', "N");
        cyrillicToLatinMap.put('Њ', "Nј");
        cyrillicToLatinMap.put('О', "O");
        cyrillicToLatinMap.put('П', "P");
        cyrillicToLatinMap.put('Р', "R");
        cyrillicToLatinMap.put('С', "S");
        cyrillicToLatinMap.put('Т', "T");
        cyrillicToLatinMap.put('Ќ', "Kj");
        cyrillicToLatinMap.put('У', "U");
        cyrillicToLatinMap.put('Ф', "F");
        cyrillicToLatinMap.put('Х', "H");
        cyrillicToLatinMap.put('Ц', "C");
        cyrillicToLatinMap.put('Ч', "Ch");
        cyrillicToLatinMap.put('Џ', "Dј");
        cyrillicToLatinMap.put('Ш', "Sh");
        cyrillicToLatinMap.put('а', "a");
        cyrillicToLatinMap.put('б', "b");
        cyrillicToLatinMap.put('в', "v");
        cyrillicToLatinMap.put('г', "g");
        cyrillicToLatinMap.put('д', "d");
        cyrillicToLatinMap.put('ѓ', "gj");
        cyrillicToLatinMap.put('е', "e");
        cyrillicToLatinMap.put('ж', "zh");
        cyrillicToLatinMap.put('з', "z");
        cyrillicToLatinMap.put('ѕ', "dz");
        cyrillicToLatinMap.put('и', "i");
        cyrillicToLatinMap.put('ј', "j");
        cyrillicToLatinMap.put('к', "k");
        cyrillicToLatinMap.put('л', "l");
        cyrillicToLatinMap.put('љ', "lj");
        cyrillicToLatinMap.put('м', "m");
        cyrillicToLatinMap.put('н', "n");
        cyrillicToLatinMap.put('њ', "nј");
        cyrillicToLatinMap.put('о', "o");
        cyrillicToLatinMap.put('п', "p");
        cyrillicToLatinMap.put('р', "r");
        cyrillicToLatinMap.put('с', "s");
        cyrillicToLatinMap.put('т', "t");
        cyrillicToLatinMap.put('ќ', "kj");
        cyrillicToLatinMap.put('у', "u");
        cyrillicToLatinMap.put('ф', "f");
        cyrillicToLatinMap.put('х', "h");
        cyrillicToLatinMap.put('ц', "c");
        cyrillicToLatinMap.put('ч', "ch");
        cyrillicToLatinMap.put('џ', "dzh");
        cyrillicToLatinMap.put('ш', "sh");

        for (Map.Entry<Character, String> entry : cyrillicToLatinMap.entrySet()) {
            latinToCyrillicMap.put(entry.getValue(), entry.getKey());
        }
    }

    public static String cyrillicToLatin(String input) {
        StringBuilder result = new StringBuilder();
        for (char ch : input.toCharArray()) {
            String transliterated = cyrillicToLatinMap.getOrDefault(ch, String.valueOf(ch));
            result.append(transliterated);
        }
        return result.toString();
    }

    public static String latinToCyrillic(String input) {
        StringBuilder result = new StringBuilder();
        int i = 0;
        while (i < input.length()) {
            String substr = input.substring(i, Math.min(i + 2, input.length()));
            if (latinToCyrillicMap.containsKey(substr)) {
                result.append(latinToCyrillicMap.get(substr));
                i += 2;
            } else {
                substr = input.substring(i, i + 1);
                result.append(latinToCyrillicMap.getOrDefault(substr, substr.charAt(0)));
                i += 1;
            }
        }
        return result.toString();
    }

    public static boolean isLatin(String input) {
        for (char ch : input.toCharArray()) {
            if (Character.UnicodeBlock.of(ch) == Character.UnicodeBlock.CYRILLIC) {
                return false;
            }
        }
        return true;
    }

    public static boolean isCyrillic(String input) {
        for (char ch : input.toCharArray()) {
            if (Character.UnicodeBlock.of(ch) == Character.UnicodeBlock.BASIC_LATIN) {
                return false;
            }
        }
        return true;
    }
}
