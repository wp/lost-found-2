package mk.ukim.finki.wp.lostfound2.service;

import mk.ukim.finki.wp.lostfound2.model.ItemComment;
import mk.ukim.finki.wp.lostfound2.model.User;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface CommentService {
    List<ItemComment> getCommentsForItem(Long itemId, User user);

    ItemComment addComment(Long itemId, String content, User author, MultipartFile imageFile);

    ItemComment addReply(Long commentId, String content, User author, MultipartFile imageFile);
}
