package mk.ukim.finki.wp.lostfound2.service.impl;

import lombok.AllArgsConstructor;
import mk.ukim.finki.wp.lostfound2.model.Item;
import mk.ukim.finki.wp.lostfound2.model.dtos.MailSendingStatus;
import mk.ukim.finki.wp.lostfound2.service.EmailService;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Value;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.Map;
import java.util.HashMap;



@Service
public class NotificationService {

    @Value("${app.base-url}")
    private String baseUrl;

    private final EmailService emailService;

    public NotificationService(EmailService emailService) {
        this.emailService = emailService;
    }

    public List<CompletableFuture<MailSendingStatus>>  notifyStudentsForSimilarItems(Item item, List<Item> itemsRelated, String subject, String template) {

        String itemLink = baseUrl + "/item/" + item.getId() + "/details";

        List<CompletableFuture<MailSendingStatus>>  mailStatuses = new ArrayList<>();

        itemsRelated.stream()
                .filter(itemEach -> itemEach.getUser() != null &&
                        itemEach.getUser().getEmail() != null)
                .map(itemEach -> itemEach.getUser().getEmail())
                .toList()
                .forEach(email -> {
                    try {
                        Map<String, Object> model = new HashMap<>();
                        model.put("itemLink", itemLink);

                        Map<String, Object> modelForMail = Map.of("itemLink", itemLink);

                        mailStatuses.add(emailService.sendMail(new String[]{email}, subject,
                                        template, null, modelForMail, null));
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                });

        return mailStatuses;
    }
}


