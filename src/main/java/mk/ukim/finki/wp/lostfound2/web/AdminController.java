package mk.ukim.finki.wp.lostfound2.web;

import jakarta.servlet.http.HttpServletRequest;
import mk.ukim.finki.wp.lostfound2.model.*;
import mk.ukim.finki.wp.lostfound2.model.exceptions.ItemNotFoundException;
import mk.ukim.finki.wp.lostfound2.service.CommentService;
import mk.ukim.finki.wp.lostfound2.service.ItemService;
import mk.ukim.finki.wp.lostfound2.service.UserService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.util.List;

@Controller
@RequestMapping("/admin")
@PreAuthorize("hasRole('ADMIN')")
public class AdminController {

    private final ItemService itemService;
    private final UserService userService;
    private final CommentService commentService;

    public AdminController(ItemService itemService, UserService userService, CommentService commentService) {
        this.itemService = itemService;
        this.userService = userService;
        this.commentService = commentService;
    }

    @GetMapping(value = {"/items", ""})
    @PreAuthorize("hasRole('ADMIN')")
    public String showListItems(
            @RequestParam(required = false, defaultValue = "1") int pageNum,
            @RequestParam(required = false, defaultValue = "10") int size,
            @RequestParam(required = false) String name,
            @RequestParam(required = false) ItemCategory itemCategory,
            @RequestParam(required = false) String description,
            @RequestParam(required = false, defaultValue = "lost") String type,
            Model model) {

        ItemType itemType = "found".equalsIgnoreCase(type) ? ItemType.FOUND : ItemType.LOST;
        Sort sort = Sort.by(Sort.Order.desc("dateIssueCreated"));
        Pageable pageable = PageRequest.of(pageNum - 1, size,sort);
        Page<Item> items = this.itemService.filter(name, itemCategory, description, itemType, pageable);

        model.addAttribute("items", items);
        model.addAttribute("category", ItemCategory.values());
        model.addAttribute("size", size);
        model.addAttribute("type", type);
        return "admin-page";
    }



    @GetMapping("/item/{id}/edit")
    public String showEdit(HttpServletRequest req,
                           @PathVariable Long id,
                           Model model) {
        Item item = itemService.findById(id);
        String username = req.getRemoteUser();
        String user_username = item.getUser().getId();
        if (!user_username.equals(username)) {
            return "redirect:/";
        }
        model.addAttribute("item", item);
        model.addAttribute("category", ItemCategory.values());
        model.addAttribute("types", ItemType.values());
        return "form";
    }

    @PostMapping("/item/{id}")
    public String update(HttpServletRequest req,
                         @PathVariable Long id,
                         @RequestParam String name,
                         @RequestParam ItemCategory itemCategory,
                         @RequestParam String description,
                         @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dateIssueCreated,
                         @RequestParam String location,
                         @RequestParam ItemType itemType,
                         @RequestParam(required = false) MultipartFile imageFile,
                         @RequestParam(required = false) boolean handover,
                         @RequestParam(required = false) String phoneNumber
    ) {

        String username = req.getRemoteUser();
        User user = userService.findByUsername(username);
        this.itemService.update(id, name, itemCategory, description, dateIssueCreated, location, itemType, imageFile, handover, user,phoneNumber);
        return "admin-page";
    }

    @GetMapping("/item/{id}/details")
    public String showDetails(
            HttpServletRequest req,
            @PathVariable Long id,
            Model model) {
        Item item = itemService.findById(id);
        if (item == null) {
            throw new ItemNotFoundException("item not found");
        }
        String username = req.getRemoteUser();
        System.out.println(req.getSession());

        String userAgent = req.getHeader("User-Agent");
        boolean isMobile = userAgent != null && userAgent.matches(".*(Android|iPhone|iPad|iPod|Windows Phone|Mobi).*");
        req.setAttribute("isMobile", isMobile);

        User user = userService.findByUsername(username);
        List<ItemComment> comments = this.commentService.getCommentsForItem(id, user);
        List<Item> similarItems = this.itemService.findSimilarItems(item);
        model.addAttribute("currentuser", user);
        model.addAttribute("item", item);
        model.addAttribute("similarItems", similarItems);
        model.addAttribute("comments", comments);
        model.addAttribute("category", ItemCategory.values());
        model.addAttribute("types", ItemType.values());
        return "details";
    }


    @GetMapping("/item/{id}/delete")
    public String delete(@PathVariable Long id) {
       Item item = this.itemService.delete(id);
        return (item.getItemType() == ItemType.FOUND)
                ? "redirect:/admin/items?type=found&name=&description=&category="
                : "redirect:/admin/items?type=lost&name=&description=&category=";
    }


    @GetMapping("/item/{id}/hide")
    public String hide(@PathVariable Long id){
        Item item = this.itemService.delete(id);
        return (item.getItemType() == ItemType.FOUND)
                ? "redirect:/found"
                : "redirect:/lost";

    }
}


