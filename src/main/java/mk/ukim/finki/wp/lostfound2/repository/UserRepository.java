package mk.ukim.finki.wp.lostfound2.repository;

import mk.ukim.finki.wp.lostfound2.model.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaSpecificationRepository<User, String> {

}
