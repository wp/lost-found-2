package mk.ukim.finki.wp.lostfound2.service.impl;

import mk.ukim.finki.wp.lostfound2.model.User;
import mk.ukim.finki.wp.lostfound2.model.exceptions.InvalidUsernameException;
import mk.ukim.finki.wp.lostfound2.repository.UserRepository;
import mk.ukim.finki.wp.lostfound2.service.UserService;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    public UserServiceImpl(UserRepository userRepository) {this.userRepository = userRepository;}

    @Override
    public User findByUsername(String username) {
        return this.userRepository.findById(username).orElseThrow(InvalidUsernameException::new);
    }

}
