package mk.ukim.finki.wp.lostfound2.repository;

import mk.ukim.finki.wp.lostfound2.model.Professor;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfessorRepository extends JpaSpecificationRepository<Professor, String> {

}
