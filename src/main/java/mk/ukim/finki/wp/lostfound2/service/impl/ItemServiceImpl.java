package mk.ukim.finki.wp.lostfound2.service.impl;

import mk.ukim.finki.wp.lostfound2.model.ItemCategory;
import mk.ukim.finki.wp.lostfound2.model.Item;
import mk.ukim.finki.wp.lostfound2.model.ItemType;
import mk.ukim.finki.wp.lostfound2.model.User;
import mk.ukim.finki.wp.lostfound2.repository.ItemRepository;
import mk.ukim.finki.wp.lostfound2.service.ItemService;
import mk.ukim.finki.wp.lostfound2.utils.ItemSpecification;
import mk.ukim.finki.wp.lostfound2.utils.LatinAndCyrillicUtils;
import mk.ukim.finki.wp.lostfound2.utils.MatchingUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class ItemServiceImpl implements ItemService {
    private final ItemRepository itemRepository;
    private final NotificationService notificationService;

    public ItemServiceImpl(ItemRepository itemRepository, NotificationService notificationService)
    {

        this.itemRepository = itemRepository;
        this.notificationService = notificationService;
    }

    @Override
    public List<Item> listAll() {
        return this.itemRepository.findAll();
    }

    @Override
    public Item findById(Long id) {
        return this.itemRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException("Item with id " + id + " not found"));
    }

    @Override
    public Item create(String name, ItemCategory itemCategory, String description, LocalDate dateIssueCreated, String location, ItemType itemType, MultipartFile imageFile, User user, String phoneNumber) {
        byte[] image;
        try {
            image = imageFile.getBytes();
        } catch (IOException e) {
            throw new RuntimeException("Failed to read file: " + e.getMessage());
        }


        Item item =  this.itemRepository.save(new Item(name, itemCategory, description, dateIssueCreated, location, itemType, image, user,phoneNumber));

        SendMailToSameLostTopic(item);

        return item;
    }

    @Override
    public Item update(Long id, String name, ItemCategory itemCategory, String description, LocalDate dateIssueCreated, String location, ItemType itemType, MultipartFile imageFile, boolean handover, User user, String phoneNumber) {
        Item item = this.itemRepository.findById(id).orElseThrow(null);
        if (!imageFile.isEmpty()) {
            byte[] image;
            try {
                image = imageFile.getBytes();
                item.setImage(image);
            } catch (IOException e) {
                throw new RuntimeException("Failed to read file: " + e.getMessage());
            }
        }

        item.setName(name);
        item.setItemCategory(itemCategory);
        item.setDescription(description);
        item.setDateIssueCreated(dateIssueCreated);
        item.setLocation(location);
        item.setItemType(itemType);
        item.setHandover(handover);
        item.setUser(user);
        item.setPhoneNumber(phoneNumber);
        return this.itemRepository.save(item);
    }

    @Override
    public Item delete(Long id) {
        Item item = findById(id);
        this.itemRepository.delete(item);
        return item;
    }

//page
    @Override
    public Page<Item> filter(String name, ItemCategory itemCategory, String description, ItemType itemType, Pageable pageable) {
        var spec = ItemSpecification.hasType(itemType)
                .and(ItemSpecification.containsName(name))
                .and(ItemSpecification.hasCategory(itemCategory))
                .and(ItemSpecification.descriptionContains(description));
        return itemRepository.findAll(spec, pageable);
    }
//page
    @Override
    public Page<Item> findByUser(User user, Pageable pageable) {
        return itemRepository.findByUser(user, pageable);
    }

    @Override
    public List<Item> findSimilarItems(Item item) {
        String name = item.getName();
        String description = item.getDescription();
        String location = item.getLocation();

        String transliteratedName = LatinAndCyrillicUtils.isLatin(name) ?
                LatinAndCyrillicUtils.latinToCyrillic(name) :
                LatinAndCyrillicUtils.cyrillicToLatin(name);

        String transliteratedDescription = LatinAndCyrillicUtils.isLatin(description) ?
                LatinAndCyrillicUtils.latinToCyrillic(description) :
                LatinAndCyrillicUtils.cyrillicToLatin(description);

        String transliteratedLocation = LatinAndCyrillicUtils.isLatin(location) ?
                LatinAndCyrillicUtils.latinToCyrillic(location) :
                LatinAndCyrillicUtils.cyrillicToLatin(location);

        ItemType oppositeItemType = item.getItemType() == ItemType.LOST ? ItemType.FOUND : ItemType.LOST;
        List<Item> oppositeItems = itemRepository.findByItemTypeAndItemCategoryAndHandoverIsFalse(oppositeItemType, item.getItemCategory());

        return oppositeItems.stream()
                .filter(oppositeItem ->
                        MatchingUtils.isItemMatch(item.getName(), oppositeItem.getName(), item.getLocation(), oppositeItem.getLocation(), item.getDescription(), oppositeItem.getDescription()) ||
                                MatchingUtils.isTransliteratedMatch(transliteratedName, oppositeItem.getName(), transliteratedLocation, oppositeItem.getLocation(), transliteratedDescription, oppositeItem.getDescription()))
                .toList();
    }
    private void SendMailToSameLostTopic (Item item){
        List<Item> similarItems = itemRepository.findByItemCategoryAndHandoverAndItemType(item.getItemCategory(),
                false, ItemType.LOST);

        notificationService.notifyStudentsForSimilarItems(item,
                similarItems,
                "Пронајден предмет од слична категорија",
                "same-topic-found-mail");
    }

}
