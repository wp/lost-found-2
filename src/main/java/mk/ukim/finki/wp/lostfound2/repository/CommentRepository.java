package mk.ukim.finki.wp.lostfound2.repository;

import mk.ukim.finki.wp.lostfound2.model.ItemComment;
import mk.ukim.finki.wp.lostfound2.model.Item;
import mk.ukim.finki.wp.lostfound2.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository extends JpaRepository<ItemComment, Long> {
    List<ItemComment> findByItemAndParentCommentIsNull(Item item);

    List<ItemComment> findByItemAndAuthorAndParentCommentIsNull(Item item, User author);

    List<ItemComment> findByParentComment(ItemComment parentComment);

    List<ItemComment> findByParentCommentAndAuthor(ItemComment parentComment, User author);
}