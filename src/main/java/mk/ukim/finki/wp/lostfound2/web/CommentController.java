package mk.ukim.finki.wp.lostfound2.web;

import jakarta.servlet.http.HttpServletRequest;
import mk.ukim.finki.wp.lostfound2.model.ItemComment;
import mk.ukim.finki.wp.lostfound2.model.User;
import mk.ukim.finki.wp.lostfound2.service.CommentService;
import mk.ukim.finki.wp.lostfound2.service.ItemService;
import mk.ukim.finki.wp.lostfound2.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/items")
public class CommentController {

    private final CommentService commentService;
    private final UserService userService;
    private final ItemService itemService;

    public CommentController(CommentService commentService, UserService userService, ItemService itemService) {
        this.commentService = commentService;
        this.userService = userService;
        this.itemService = itemService;
    }


    @PostMapping("addcomment/{itemId}")
    public String addComment(HttpServletRequest req,
                             @PathVariable Long itemId,
                             @RequestParam String content,
                             @RequestParam(required = false) MultipartFile imageFile
    ) {
        String username = req.getRemoteUser();
        User user = userService.findByUsername(username);
        commentService.addComment(itemId, content, user,imageFile);
        return "redirect:/item/" + itemId + "/details";
    }

    @PostMapping("/comments/{commentId}/reply")
    public String addReply(HttpServletRequest req,
                           @PathVariable Long commentId,
                           @RequestParam String content,
                           @RequestParam(required = false) MultipartFile imageFile) {
        String username = req.getRemoteUser();
        User user = userService.findByUsername(username);
        ItemComment parentComment = commentService.addReply(commentId, content, user, imageFile);
        return "redirect:/item/" + parentComment.getItem().getId() + "/details";
    }
}
