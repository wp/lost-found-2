package mk.ukim.finki.wp.lostfound2.model.exceptions;


public class ItemNotFoundException extends RuntimeException {
    public ItemNotFoundException(String message) {
        super(message);
    }
}