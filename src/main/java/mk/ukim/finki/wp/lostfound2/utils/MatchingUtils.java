package mk.ukim.finki.wp.lostfound2.utils;

import org.apache.commons.text.similarity.FuzzyScore;

import java.util.Locale;

public class MatchingUtils {

    private static String normalize(String input) {
        return input.toLowerCase().replaceAll("[^a-z0-9 ]", "").trim();
    }

    private static boolean isPartialMatch(String token1, String token2) {
        String[] tokens1 = normalize(token1).split("\\s+");
        String[] tokens2 = normalize(token2).split("\\s+");

        for (String t1 : tokens1) {
            for (String t2 : tokens2) {
                if (t2.contains(t1) || t1.contains(t2)) {
                    return true;
                }
            }
        }
        return false;
    }

    private static boolean isFuzzyMatch(String str1, String str2) {
        FuzzyScore fuzzyScore = new FuzzyScore(Locale.ENGLISH);
        int score = fuzzyScore.fuzzyScore(normalize(str1), normalize(str2));
        int threshold = Math.min(normalize(str1).length(), normalize(str2).length()) / 2;

        return score > threshold;
    }

    private static boolean isMatch(String str1, String str2) {
        return isPartialMatch(str1, str2) || isFuzzyMatch(str1, str2);
    }

    public static boolean isItemMatch(String lostItemName, String foundItemName, String lostItemLocation, String foundItemLocation, String lostItemDescription, String foundItemDescription) {
        return isMatch(lostItemName, foundItemName) &&
                isMatch(lostItemLocation, foundItemLocation) &&
                isMatch(lostItemDescription, foundItemDescription);
    }

    public static boolean isTransliteratedMatch(String nameTransliterated,
                                                String nameOriginal,
                                                String locationTransliterated,
                                                String locationOriginal,
                                                String descriptionTransliterated,
                                                String descriptionOriginal) {
        return isMatch(nameTransliterated, nameOriginal) &&
                isMatch(locationTransliterated, locationOriginal) &&
                isMatch(descriptionTransliterated, descriptionOriginal);
    }
}

