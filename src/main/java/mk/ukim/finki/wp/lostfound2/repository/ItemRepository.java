package mk.ukim.finki.wp.lostfound2.repository;

import mk.ukim.finki.wp.lostfound2.model.ItemCategory;
import mk.ukim.finki.wp.lostfound2.model.Item;
import mk.ukim.finki.wp.lostfound2.model.ItemType;
import mk.ukim.finki.wp.lostfound2.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemRepository extends JpaSpecificationRepository<Item, Long>, JpaSpecificationExecutor<Item> {
    Page<Item> findByItemTypeAndHandoverIsFalse(ItemType itemType, Pageable pageable);//p

    Page<Item> findByUser(User user, Pageable pageable);//p

    List<Item> findByItemTypeAndItemCategoryAndHandoverIsFalse(ItemType itemType, ItemCategory itemCategory);

    List<Item> findByItemCategoryAndHandoverAndItemType(ItemCategory itemCategory, Boolean handover, ItemType itemType);

}
