package mk.ukim.finki.wp.lostfound2.service.impl;

import mk.ukim.finki.wp.lostfound2.model.ItemComment;
import mk.ukim.finki.wp.lostfound2.model.Item;
import mk.ukim.finki.wp.lostfound2.model.User;
import mk.ukim.finki.wp.lostfound2.model.exceptions.CommentNotFound;
import mk.ukim.finki.wp.lostfound2.model.exceptions.ItemNotFoundException;
import mk.ukim.finki.wp.lostfound2.repository.CommentRepository;
import mk.ukim.finki.wp.lostfound2.repository.ItemRepository;
import mk.ukim.finki.wp.lostfound2.service.CommentService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {

    private final CommentRepository commentRepository;
    private final ItemRepository itemRepository;

    public CommentServiceImpl(CommentRepository commentRepository, ItemRepository itemRepository) {
        this.commentRepository = commentRepository;
        this.itemRepository = itemRepository;
    }

    @Override
    public List<ItemComment> getCommentsForItem(Long itemId, User user) {
        Item item = itemRepository.findById(itemId)
                .orElseThrow(() -> new ItemNotFoundException("Item not found"));

        boolean isOwner = item.getUser().equals(user);
        List<ItemComment> comments;

        if (isOwner) {
            comments = commentRepository.findByItemAndParentCommentIsNull(item);
        } else {
            comments = commentRepository.findByItemAndAuthorAndParentCommentIsNull(item, user);
        }

        loadReplies(comments, user, isOwner);
        return comments;
    }

    private void loadReplies(List<ItemComment> comments, User user, boolean isOwner) {
        for (ItemComment comment : comments) {
            List<ItemComment> replies;

            replies = commentRepository.findByParentComment(comment);

            comment.setReplies(replies);

            if (!replies.isEmpty()) {
                loadReplies(replies, user, isOwner);
            }
        }
    }

    @Override
    public ItemComment addComment(Long itemId, String content, User author, MultipartFile imageFile) {
        Item item = itemRepository.findById(itemId)
                .orElseThrow(() -> new ItemNotFoundException("Item not found"));
        ItemComment comment = new ItemComment();
        byte[] image;
        if (imageFile != null) {
            try {
                image = imageFile.getBytes();
            } catch (IOException e) {
                throw new RuntimeException("Failed to read file: " + e.getMessage());
            }
        } else {
            image = null;
        }
        comment.setContent(content);
        comment.setAuthor(author);
        comment.setItem(item);
        comment.setDateTime(LocalDateTime.now());
        comment.setImage(image);

        return commentRepository.save(comment);
    }

    @Override
    public ItemComment addReply(Long commentId, String content, User author, MultipartFile imageFile) {
        ItemComment parentComment = commentRepository.findById(commentId)
                .orElseThrow(() -> new CommentNotFound("Comment not found"));
        ItemComment reply = new ItemComment();
        byte[] image;
        if (imageFile != null) {
            try {
                image = imageFile.getBytes();
            } catch (IOException e) {
                throw new RuntimeException("Failed to read file: " + e.getMessage());
            }
        } else {
            image = null;
        }
        reply.setContent(content);
        reply.setAuthor(author);
        reply.setItem(parentComment.getItem());
        reply.setParentComment(parentComment);
        reply.setDateTime(LocalDateTime.now());
        reply.setImage(image);
        return commentRepository.save(reply);
    }
}
