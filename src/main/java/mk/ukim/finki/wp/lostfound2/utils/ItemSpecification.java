package mk.ukim.finki.wp.lostfound2.utils;

import mk.ukim.finki.wp.lostfound2.model.ItemCategory;
import mk.ukim.finki.wp.lostfound2.model.Item;
import mk.ukim.finki.wp.lostfound2.model.ItemType;
import org.springframework.util.StringUtils;
import org.springframework.data.jpa.domain.Specification;

public class ItemSpecification {

    public static Specification<Item> containsName(String name) {
        return (root, query, criteriaBuilder) ->
                StringUtils.hasText(name)
                        ? criteriaBuilder.like(criteriaBuilder.lower(root.get("name")), "%" + name.toLowerCase() + "%")
                        : null;
    }

    public static Specification<Item> descriptionContains(String keyword) {
        return (root, query, criteriaBuilder) ->
                StringUtils.hasText(keyword)
                        ? criteriaBuilder.like(criteriaBuilder.lower(root.get("description")), "%" + keyword.toLowerCase() + "%")
                        : null;
    }


    public static Specification<Item> hasCategory(ItemCategory itemCategory){
        return (root, query, criteriaBuilder) -> itemCategory != null ? criteriaBuilder.equal(root.get("itemCategory"), itemCategory) : null;
    }

    public static Specification<Item> hasType(ItemType itemType) {
        return (root, query, criteriaBuilder) ->
                itemType != null ? criteriaBuilder.equal(root.get("itemType"), itemType) : null;
    }

}
