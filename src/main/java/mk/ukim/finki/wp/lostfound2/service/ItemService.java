package mk.ukim.finki.wp.lostfound2.service;

import mk.ukim.finki.wp.lostfound2.model.ItemCategory;
import mk.ukim.finki.wp.lostfound2.model.Item;
import mk.ukim.finki.wp.lostfound2.model.ItemType;
import mk.ukim.finki.wp.lostfound2.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.util.List;


public interface ItemService {

    List<Item> listAll();

    Item findById(Long id);

    Item create(String name, ItemCategory itemCategory, String description, LocalDate dateIssueCreated, String location, ItemType itemType, MultipartFile file, User user, String phoneNumber);

    Item update(Long id, String name, ItemCategory itemCategory, String description, LocalDate dateIssueCreated, String location, ItemType itemType, MultipartFile file, boolean handover, User user, String phoneNumber);

    Item delete(Long id);

    Page<Item> filter(String name, ItemCategory itemCategory, String description, ItemType itemType, Pageable pageable);//p

    Page<Item> findByUser(User user, Pageable pageable);//p

    List<Item> findSimilarItems(Item item);
}
