package mk.ukim.finki.wp.lostfound2.model;

public enum RoomType {

    CLASSROOM, LAB, MEETING_ROOM, OFFICE, VIRTUAL
}
