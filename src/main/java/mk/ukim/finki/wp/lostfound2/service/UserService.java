package mk.ukim.finki.wp.lostfound2.service;

import mk.ukim.finki.wp.lostfound2.model.User;

public interface UserService {
    User findByUsername(String username);

}
