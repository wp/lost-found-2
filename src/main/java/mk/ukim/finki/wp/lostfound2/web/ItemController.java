package mk.ukim.finki.wp.lostfound2.web;

import jakarta.servlet.http.HttpServletRequest;
import mk.ukim.finki.wp.lostfound2.config.FacultyUserDetails;
import mk.ukim.finki.wp.lostfound2.model.*;
import mk.ukim.finki.wp.lostfound2.model.exceptions.ItemNotFoundException;
import mk.ukim.finki.wp.lostfound2.service.CommentService;
import mk.ukim.finki.wp.lostfound2.service.ItemService;
import mk.ukim.finki.wp.lostfound2.service.UserService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.util.List;

@Controller
public class ItemController {

    private final ItemService itemService;
    private final UserService userService;
    private final CommentService commentService;

    public ItemController(ItemService itemService, UserService userService, CommentService commentService) {
        this.itemService = itemService;
        this.userService = userService;
        this.commentService = commentService;
    }

    @GetMapping("/my-reports")
    public String showMyReports(@RequestParam(required = false, defaultValue = "1") int pageNum,
                                @RequestParam(required = false, defaultValue = "10") int size,
                                HttpServletRequest req,
                                Pageable pageable,
                                Model model) {
        String username = req.getRemoteUser();
        User user = userService.findByUsername(username);
        System.out.println(req.getSession());
        Page<Item> reportedItems = itemService.findByUser(user, PageRequest.of(pageNum - 1, size));
        model.addAttribute("reportedItems", reportedItems);
        return "my-reports";
    }

    @GetMapping(value = {"/lost"})
    public String showListLost(@RequestParam(required = false, defaultValue = "1") int pageNum,
                               @RequestParam(required = false, defaultValue = "10") int size,
                               @RequestParam(required = false) String name,
                               @RequestParam(required = false) ItemCategory itemCategory,
                               @RequestParam(required = false) String description,
                               Model model,
                               Authentication token) {

        Pageable pageable = PageRequest.of(pageNum - 1, size);
        Page<Item> items = this.itemService.filter(name, itemCategory, description, ItemType.LOST, pageable);

        Object principal = token.getPrincipal();
        FacultyUserDetails userAcc = (FacultyUserDetails) principal;
        String username = userAcc.getUsername();

        User user = userService.findByUsername(username);

        model.addAttribute("isAdmin", (user.getRole().getApplicationRole() == AppRole.ADMIN));
        model.addAttribute("items", items);
        model.addAttribute("category", ItemCategory.values());
        model.addAttribute("size", size);
        return "lost";
    }

    @GetMapping(value = {"/found"})
    public String showListFound(@RequestParam(required = false, defaultValue = "1") int pageNum,
                                @RequestParam(required = false, defaultValue = "10") int size,
                                @RequestParam(required = false) String name,
                                @RequestParam(required = false) ItemCategory itemCategory,
                                @RequestParam(required = false) String description,
                                Model model,
                                Authentication token) {

        Pageable pageable = PageRequest.of(pageNum - 1, size);
        Page<Item> items = this.itemService.filter(name, itemCategory, description, ItemType.FOUND, pageable);

        Object principal = token.getPrincipal();
        FacultyUserDetails userAcc = (FacultyUserDetails) principal;
        String username = userAcc.getUsername();

        User user = userService.findByUsername(username);

        model.addAttribute("isAdmin", (user.getRole().getApplicationRole() == AppRole.ADMIN));
        model.addAttribute("items", items);
        model.addAttribute("category", ItemCategory.values());
        model.addAttribute("size", size);
        return "found";
    }

    @GetMapping("/item/add")
    public String showAdd(Model model) {
        model.addAttribute("item", new Item());
        model.addAttribute("category", ItemCategory.values());
        model.addAttribute("types", ItemType.values());
        return "form";
    }


    @GetMapping("/item/{id}/edit")
    public String showEdit(HttpServletRequest req,
                           @PathVariable Long id,
                           Model model) {
        Item item = itemService.findById(id);
        String username = req.getRemoteUser();
        String user_username = item.getUser().getId();
        if (!user_username.equals(username)) {
            return "redirect:/";
        }
        model.addAttribute("item", item);
        model.addAttribute("category", ItemCategory.values());
        model.addAttribute("types", ItemType.values());
        return "form";
    }

    @GetMapping("/item/{id}/details")
    public String showDetails(
//                                Principal principal,
            HttpServletRequest req,
            @PathVariable Long id,
            Model model) {
        Item item = itemService.findById(id);
        if (item == null) {
            throw new ItemNotFoundException("item not found");
        }
        String username = req.getRemoteUser();

        System.out.println(req.getSession());

        String userAgent = req.getHeader("User-Agent");
        boolean isMobile = userAgent != null && userAgent.matches(".*(Android|iPhone|iPad|iPod|Windows Phone|Mobi).*");
        req.setAttribute("isMobile", isMobile);
//        String username = principal.getName();
        User user = userService.findByUsername(username);
        List<ItemComment> comments = this.commentService.getCommentsForItem(id, user);
        List<Item> similarItems = this.itemService.findSimilarItems(item);
        model.addAttribute("currentuser", user);
        model.addAttribute("item", item);
        model.addAttribute("similarItems", similarItems);
        model.addAttribute("comments", comments);
        model.addAttribute("category", ItemCategory.values());
        model.addAttribute("types", ItemType.values());
        return "details";
    }

    @PostMapping("/item")
    public String create(HttpServletRequest req,
                         @RequestParam String name,
                         @RequestParam ItemCategory itemCategory,
                         @RequestParam String description,
                         @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dateIssueCreated,
                         @RequestParam String location,
                         @RequestParam ItemType itemType,
                         @RequestParam(required = false) MultipartFile imageFile,
                         @RequestParam(required = false) String phoneNumber
    ) {
        String username = req.getRemoteUser();
        User user = userService.findByUsername(username);
        if (phoneNumber.isEmpty()){
            phoneNumber=null;
        }

        this.itemService.create(name, itemCategory, description, dateIssueCreated, location, itemType, imageFile, user, phoneNumber);
        if (itemType == ItemType.LOST) {
            return "redirect:/lost";
        } else
            return "redirect:/found";

    }


    @PostMapping("/item/{id}")
    public String update(HttpServletRequest req,
                         @PathVariable Long id,
                         @RequestParam String name,
                         @RequestParam ItemCategory itemCategory,
                         @RequestParam String description,
                         @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dateIssueCreated,
                         @RequestParam String location,
                         @RequestParam ItemType itemType,
                         @RequestParam(required = false) MultipartFile imageFile,
                         @RequestParam(required = false) boolean handover,
                         @RequestParam(required = false) String phoneNumber
    ) {

        String username = req.getRemoteUser();
        User user = userService.findByUsername(username);
        if (phoneNumber.isEmpty()){
            phoneNumber=null;
        }
        this.itemService.update(id, name, itemCategory, description, dateIssueCreated, location, itemType, imageFile, handover, user,phoneNumber);
        if (itemType == ItemType.LOST) {
            return "redirect:/lost";
        } else
            return "redirect:/found";

    }

    @PostMapping("/item/{id}/delete")
    public String delete(HttpServletRequest req,
                         @PathVariable Long id) {
        Item item = itemService.findById(id);
        String username = req.getRemoteUser();
        String user_username = item.getUser().getId();
        if (!user_username.equals(username)) {
            return "redirect:/";
        }
        ItemType itemType = itemService.findById(id).getItemType();
        this.itemService.delete(id);
        if (itemType == ItemType.LOST) {
            return "redirect:/lost";
        } else
            return "redirect:/found";
    }
}


